// Conditions - statements that will either result to true or false

// if(condition)

// const age = 15;
// function checkAge(age){
// 	if(age>18){
// 		return "legal ka na."
// 	};
// };

// function checkAge(age){
// 	if(age>=18){
// 		return "Legal ka na"
// 	}else{
// 		return "minor ka pa";
// 	}
// }

// // This function checks the age and returns a string value
// // @param {number} age user-input age

function whereYouShouldBe(age){
	// 0-5 with Mommy/daddy
	// 6-12 Elementary School
	// 13-18 High School
	// 19-22 College
	// 22-23 Soul Searching
	// 24-50 Work
	// 51-Enjoying life
	if (age <= 5){
		return "With Mommy/daddy";
	}else if(age <= 12){
		return "Elementary School";
	}else if(age <= 22){
		return "College";
	}else if(age <= 23){
		return ("Soul Searching");
	}else if(age <= 50){
		return "Work";
	}else if(age > 51){
		return "Enjoying Life";
	};
}