
// 1. Create an array of words where we will get the converted value
//Goal 1: Convert 0-9;
const oneDigit = ["Zero", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine"];

//Goal 2: Convert 10-90 (Multiples of 10) 
// 10, 20, 30, 40, 50, 60, 70, 80, 90
// Divide the data by 10, and you'll get the index-1;
// So we added a fake data at the beginning
const tens = ["", "Ten", "Twenty", "Thirty", "Forty", "Fifty", "Sixty", "Seventy", "Eighty", "Ninety"];

//Goal 3: We have to make sure that Goal#2 will only check numbers less than 100
//
//Goal 4: 11, 12, 13, 14, 15, 16, 17, 18, 19
const teens = ["", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen", "Nineteen"];

//Goal 5: All other numbers less than 100.

function converter(number) {
	//Goal 1
	if( number <= 9 ){
		return oneDigit[number];
	} 

	//Goal 2
	//to check if a number is a multiple of ten, the remainder when the number is divided by 10 should be zero;
	//Goal 3
	//We added another condition to make sure that the number will be divisible by 10 AND less than 100.
	if (number%10 === 0 && number < 100){
		//We can get the index by dividing the number by 10
		const tensIndex = number/10;
		return tens[tensIndex];
	}

	//Goal 4
	if( number > 10 && number < 20) {
		//We can get the "index" by subtracting 10 from the number.
		const teensIndex = number - 10;
		return teens[teensIndex];
	}

	//Goal 5, - 3/4 --- thirty four
	//			5/6 --- fifty six
	//			8/3	--- eighty three
	//The goal of this function is to separate the first digit (tensIndex) and the last digit (onesIndex). Use them as an index to get the value from the previously created arrays. 
	if( number < 100 ){
		//to get the last digit, we will get the remainder of the number when divided by 10 (%10)
		const onesIndex = number % 10;
		//to get the first digit, we need to divide the number by 10. BUT make sure first that there will be no remainders.
		const numberWithoutRemainder = number - onesIndex;
		const tensIndex = numberWithoutRemainder / 10;
		return tens[tensIndex] + " " +oneDigit[onesIndex];
	}


	//the keyword return will terminate the function so therefore the tasks below the return statement will not be executed.


// Activity
// Continue the converter.

//(ALL DIVISIBLE BY TEN)
	
	// Divisible by 1000 and Modulo is 100
	if (number%100 === 0 && number < 1000){
			const hundredIndex = number/100;
			return oneDigit[hundredIndex] + " Hundred";
		}

		// Equals or Less than 990 divisible by 10
	if(number > 100 && number % 10 === 0 && number <= 990){
			const thirdDigit = number % 100;
			const secondDigit = number % 10;
			const thirdNoRemainder = number - thirdDigit;
			const thirdHundredDigit = thirdNoRemainder / 100;
			const secondTensDigit = thirdDigit / 10;
			return oneDigit[thirdHundredDigit] + " Hundred " + tens[secondTensDigit];
	}

	//Divisible by 1000 and Modulo is Zero
	if (number%1000 === 0 && number < 10000){
			const thousandIndex = number/1000;
			return oneDigit[thousandIndex] + " thousand";
	}

	// Equals or Less than 9,990 divisible by 10
	if(number > 1000 && number % 10 === 0 && number <= 9990){
			const fourthDigit = number % 1000;
			const thirdDigit = number % 100;
			const secondDigit = number % 10;
			const fourthNoRemainder = number - fourthDigit;
			const fourthThousandDigit = fourthNoRemainder / 1000;
			const thirdNoRemainder = fourthDigit - thirdDigit;
			const thirdHundredDigit = thirdNoRemainder / 100;
			const secondTensDigit = thirdDigit / 10;
				if( thirdHundredDigit === 0){
					return oneDigit[fourthThousandDigit] + " Thousand " + tens[secondTensDigit];
				}
				else{
					return oneDigit[fourthThousandDigit] + " Thousand " + oneDigit[thirdHundredDigit] + " Hundred " + tens[secondTensDigit];
				}
	}

	// Less than 10000 divisible by 10

	//TEN THOUSAND
	if (number%10000 === 0 && number < 100000){
			const thousandIndex = number/10000;
			return tens[thousandIndex] + " thousand";
	}

	// Equals or Less than 99,990 divisible by 10
	if(number > 10000 && number % 10 === 0 && number <= 99990){
			const fifthDigit = number % 10000;
			const fourthDigit = number % 1000;
			const thirdDigit = number % 100;
			const secondDigit = number % 10;
			const fifthNoRemainder = number - fifthDigit
			const fifthTenThouDigit = fifthNoRemainder / 10000;
			const fourthNoRemainder = fifthDigit - fourthDigit;
			const fourthThousandDigit = fourthNoRemainder / 1000;
			const thirdNoRemainder = fourthDigit - thirdDigit;
			const thirdHundredDigit = thirdNoRemainder / 100;
			const secondTensDigit = thirdDigit / 10;
			if( fourthThousandDigit === 0 && thirdHundredDigit === 0){
				return tens[fifthTenThouDigit]  + " Thousand " + tens[secondTensDigit];
			}
			if( thirdHundredDigit === 0){
					return tens[fifthTenThouDigit] + " " + oneDigit[fourthThousandDigit] + " Thousand " + tens[secondTensDigit];
			}
				
			if( fourthThousandDigit === 0 ){
				return tens[fifthTenThouDigit]  + " Thousand " + oneDigit[thirdHundredDigit] + " Hundred " + tens[secondTensDigit];
			}

			else{
					return tens[fifthTenThouDigit] + " " + oneDigit[fourthThousandDigit] + " Thousand " + oneDigit[thirdHundredDigit] + " Hundred " + tens[secondTensDigit];
			}
	}

	// HUNDRED THOUSAND
	if (number%100000 === 0 && number < 1000000){
			const thousandIndex = number/100000;
			return oneDigit[thousandIndex] + " hundred thousand";
	}

	// Equals or Less than 99,990 divisible by 10
		// Ex 92,230 Nine Two Thousand two hundred Thirty Two
	if(number > 100000 && number % 10 === 0 && number <= 999990){
			const sixthDigit = number % 100000;
			const fifthDigit = number % 10000;
			const fourthDigit = number % 1000;
			const thirdDigit = number % 100;
			const secondDigit = number % 10;
			const sixthNoRemainder = number - sixthDigit;
			const sixthHundredThouDigit = sixthNoRemainder / 100000;
			const fifthNoRemainder = sixthDigit - fifthDigit;
			const fifthTenThouDigit = fifthNoRemainder / 10000;
			const fourthNoRemainder = fifthDigit - fourthDigit;
			const fourthThousandDigit = fourthNoRemainder / 1000;
			const thirdNoRemainder = fourthDigit - thirdDigit;
			const thirdHundredDigit = thirdNoRemainder / 100;
			const secondTensDigit = thirdDigit / 10;
			if( thirdHundredDigit === 0){
				return oneDigit[sixthHundredThouDigit] + " Hundred "   + tens[fifthTenThouDigit] + " " + oneDigit[fourthThousandDigit] + " Thousand " + tens[secondTensDigit];
			}
			if( thirdHundredDigit === 0 && fourthThousandDigit === 0 ){
				return oneDigit[sixthHundredThouDigit] + " Hundred "   + tens[fifthTenThouDigit]  + " Thousand " + tens[secondTensDigit];
			}
			if( thirdHundredDigit === 0 && fifthTenThouDigitt === 0 ){
				return oneDigit[sixthHundredThouDigit] + " Hundred "   + oneDigit[fourthThousandDigit] + " Thousand " + tens[secondTensDigit];
			}
			if( fourthThousandDigit === 0 ){
				return oneDigit[sixthHundredThouDigit] + " Hundred "   + tens[fifthTenThouDigit]  + " Thousand " + oneDigit[thirdHundredDigit] + " Hundred " + tens[secondTensDigit];
			}
			if( fourthThousandDigit === 0 && fifthTenThouDigitt === 0 ){
				return oneDigit[sixthHundredThouDigit] + " Hundred "   + oneDigit[fourthThousandDigit] + " Thousand " + oneDigit[thirdHundredDigit] + " Hundred " + tens[secondTensDigit];
			}
			if( fifthTenThouDigit === 0 ){
				return oneDigit[sixthHundredThouDigit] + " Hundred "   + oneDigit[fourthThousandDigit] + " Thousand " + oneDigit[thirdHundredDigit] + " Hundred " + tens[secondTensDigit];
			}
			else{
				return oneDigit[sixthHundredThouDigit] + " Hundred "   + tens[fifthTenThouDigit] + " " + oneDigit[fourthThousandDigit] + " Thousand " + oneDigit[thirdHundredDigit] + " Hundred " + tens[secondTensDigit];
			}
	}

	//MILLION
	if (number%1000000 === 0 && number < 10000000){
			const millionIndex = number/1000000;
			return oneDigit[millionIndex] + " million";
		}

	// Equals or Less than 999,990 divisible by 10
	if(number > 1000000 && number % 10 === 0 && number <= 9999990){
			const seventhDigit = number % 1000000;
			const sixthDigit = number % 100000;
			const fifthDigit = number % 10000;
			const fourthDigit = number % 1000;
			const thirdDigit = number % 100;
			const secondDigit = number % 10;
			const seventhNoRemainder = number - seventhDigit;
			const seventhMilNiumber = seventhNoRemainder / 1000000;
			const sixthNoRemainder = seventhDigit - sixthDigit;
			const sixthHundredThouDigit = sixthNoRemainder / 100000;
			const fifthNoRemainder = sixthDigit - fifthDigit
			const fifthTenThouDigit = fifthNoRemainder / 10000;
			const fourthNoRemainder = fifthDigit - fourthDigit;
			const fourthThousandDigit = fourthNoRemainder / 1000;
			const thirdNoRemainder = fourthDigit - thirdDigit;
			const thirdHundredDigit = thirdNoRemainder / 100;
			const secondTensDigit = thirdDigit / 10;
			if( thirdHundredDigit === 0){
				return oneDigit[seventhMilNiumber] + " Million " + oneDigit[sixthHundredThouDigit] + " Hundred "   + tens[fifthTenThouDigit] + " " + oneDigit[fourthThousandDigit] + " Thousand " + tens[secondTensDigit];
			}
			if( fourthThousandDigit === 0 ){
				return oneDigit[seventhMilNiumber] + " Million " + oneDigit[sixthHundredThouDigit] + " Hundred "   + tens[fifthTenThouDigit] + " Thousand " + oneDigit[thirdHundredDigit] + " Hundred " + tens[secondTensDigit];
			}
			if( fifthTenThouDigit === 0 ){
				return oneDigit[seventhMilNiumber] + " Million " + oneDigit[sixthHundredThouDigit] + " Hundred "  + oneDigit[fourthThousandDigit] + " Thousand " + oneDigit[thirdHundredDigit] + " Hundred " + tens[secondTensDigit];
			}
			if( sixthHundredThouDigit === 0 ){
				return oneDigit[seventhMilNiumber] + " Million "  + tens[fifthTenThouDigit] + " " + oneDigit[fourthThousandDigit] + " Thousand " + oneDigit[thirdHundredDigit] + " Hundred " + tens[secondTensDigit];
			}
			
			else{
				return oneDigit[seventhMilNiumber] + " Million " + oneDigit[sixthHundredThouDigit] + " Hundred "   + tens[fifthTenThouDigit] + " " + oneDigit[fourthThousandDigit] + " Thousand " + oneDigit[thirdHundredDigit] + " Hundred " + tens[secondTensDigit];
			}
			
	}


	//TEN MILLION
	if (number%10000000 === 0 && number < 100000000){
			const tenMillionIndex = number/10000000;
			return tens[tenMillionIndex] + " million";
		}
	//HUNDRED MILLION
	if (number%100000000 === 0 && number < 1000000000){
			const hundredMillionIndex = number/100000000;
			return oneDigit[hundredMillionIndex] + " hundred million";
		}



	/////NOT DIVISIBLE BY TEN
// GOAL 6
		// / 9/4/1  --- Nine Hundred Forty One
// To get a P - convert until 999.
	if( number < 1000 ){
		//to get the two digit tens and last digit
		const twoDigit = number % 100;
		//to get the first digit 
		const firstDigit = twoDigit % 10;
		//to get the second digit
		const tensNoRemainder = twoDigit - firstDigit;
		const tensIndex = tensNoRemainder / 10;
		//tog get the hundres
		const hundredDigit = number - twoDigit;
		const hundredIndex = hundredDigit / 100;

			if ( twoDigit > 10 && twoDigit < 20){
				const teensNumber = twoDigit - 10;
				return oneDigit[hundredIndex] + " hundred " + teens[teensNumber]; 
			}
			if( tensIndex === 0){
				return oneDigit[hundredIndex] + " hundred " +oneDigit[firstDigit];
			}
			else{
				return oneDigit[hundredIndex] + " hundred " + tens[tensIndex] + " " +oneDigit[firstDigit];
			}
	}



// GOAL 7
	// 9/2/3/1 - Nine Thousand Two Hundred Thirty One
// To get an S - convert until 9999.
if( number < 10000 ){
		
		const threeDigit = number % 1000;
		const twoDigits	= threeDigit % 100;
		const oneNumber = twoDigits % 10;
		const thousandNoRemainder = number - threeDigit;
		const thousandDigit = thousandNoRemainder / 1000;
		const hundredsNoRemainder = threeDigit - twoDigits;
		const hundredsDigit = hundredsNoRemainder / 100;
		const byTenNoRemainer = twoDigits - oneNumber;
		const byTensNumber = byTenNoRemainer / 10;
			if(hundredsDigit === 0 && byTensNumber === 0){
				return oneDigit[thousandDigit] + " thousand " + " " +oneDigit[oneNumber];
			}

			if ( twoDigits > 10 && twoDigits < 20){
				const teensNumber = twoDigits - 10;
				return oneDigit[thousandDigit] + " thousand " + oneDigit[hundredsDigit] + " hundred " + teens[teensNumber];
			}
			if( byTensNumber === 0){
				return oneDigit[thousandDigit] + " thousand " + oneDigit[hundredsDigit] + " hundred " + " " +oneDigit[oneNumber];
			}
			if(hundredsDigit === 0){
				return oneDigit[thousandDigit] + " thousand " + tens[byTensNumber] + " " +oneDigit[oneNumber];
			}
						else{
				return oneDigit[thousandDigit] + " thousand " + oneDigit[hundredsDigit] + " hundred " + tens[byTensNumber] + " " +oneDigit[oneNumber];
			}
	}


// Zuitt's Record - 999, 999, 999.

// For 99,999
if( number < 100000 ){
		
		const fourDigit = number % 10000;
		const threeDigit = number % 1000;
		const twoDigits	= number % 100;
		const oneNumber = number % 10;
		const tenThousandNoRemainder = number - fourDigit;
		const tenThousandDigit = tenThousandNoRemainder / 10000;
		const thousandNoRemainder = fourDigit - threeDigit;
		const thousandDigit = thousandNoRemainder / 1000;
		const hundredsNoRemainder = threeDigit - twoDigits;
		const hundredsDigit = hundredsNoRemainder / 100;
		const byTenNoRemainer = twoDigits - oneNumber;
		const byTensNumber = byTenNoRemainer / 10;
			if ( twoDigits > 10 && twoDigits < 20){
				const teensNumber = twoDigits - 10;
				return tens[tenThousandDigit] + " " + oneDigit[thousandDigit] + " thousand " + oneDigit[hundredsDigit] + " hundred " + teens[teensNumber];
			}
			if(byTensNumber === 0 && hundredsDigit === 0 && thousandDigit === 0 ){
				return tens[tenThousandDigit] + " Thousand " + oneDigit[oneNumber];
			}
			if(byTensNumber === 0 && hundredsDigit === 0){
				return tens[tenThousandDigit] + " " + oneDigit[thousandDigit] + " thousand " + " " +oneDigit[oneNumber];
			}
			if(byTensNumber === 0 && thousandDigit === 0){
				return tens[tenThousandDigit] + " " + oneDigit[hundredsDigit] + " hundred " + " " +oneDigit[oneNumber];
			}
			if(hundredsDigit === 0 && thousandDigit === 0){
				return tens[tenThousandDigit] + " Thousand " + tens[byTensNumber] + " " +oneDigit[oneNumber];
			}
			if(byTensNumber === 0){
				return tens[tenThousandDigit] + " " + oneDigit[thousandDigit] + " thousand " + oneDigit[hundredsDigit] + " hundred " + " " + oneDigit[oneNumber];
			}
			if(hundredsDigit === 0){
				return tens[tenThousandDigit] + " " + oneDigit[thousandDigit] + " thousand " + tens[byTensNumber] + " " +oneDigit[oneNumber];
			}
			if(thousandDigit === 0){
				return tens[tenThousandDigit] + " " + oneDigit[hundredsDigit] + " hundred " + tens[byTensNumber] + " " +oneDigit[oneNumber];
			}
			else{
				return tens[tenThousandDigit] + " " + oneDigit[thousandDigit] + " thousand " + oneDigit[hundredsDigit] + " hundred " + tens[byTensNumber] + " " + oneDigit[oneNumber];
			}
			
	}

// For 999,999
if( number < 1000000 ){
		const fiveDigit = number % 100000;
		const fourDigit = number % 10000;
		const threeDigit = number % 1000;
		const twoDigits	= number % 100;
		const oneNumber = number % 10;
		const hundredThousandNoRemainder = number - fiveDigit;
		const hundredThousandDigit = hundredThousandNoRemainder / 100000;
		const tenThousandNoRemainder = fiveDigit - fourDigit;
		const tenThousandDigit = tenThousandNoRemainder / 10000;
		const thousandNoRemainder = fourDigit - threeDigit;
		const thousandDigit = thousandNoRemainder / 1000;
		const hundredsNoRemainder = threeDigit - twoDigits;
		const hundredsDigit = hundredsNoRemainder / 100;
		const byTenNoRemainer = twoDigits - oneNumber;
		const byTensNumber = byTenNoRemainer / 10;
			if ( twoDigits > 10 && twoDigits < 20){
				const teensNumber = twoDigits - 10;
				return oneDigit[hundredThousandDigit] + " Hundred " + tens[tenThousandDigit] + " " + oneDigit[thousandDigit] + " Thousand " + oneDigit[hundredsDigit] + " Hundred " + teens[teensNumber];
			}
			if( byTensNumber === 0 && hundredsDigit === 0 && thousandDigit === 0 && tenThousandDigit === 0){
				return oneDigit[hundredThousandDigit] + " Hundred " + " Thousand " + oneDigit[oneNumber];
			}

			if( hundredsDigit === 0 && thousandDigit === 0 && byTensNumber === 0 ){
				return oneDigit[hundredThousandDigit] + " Hundred Thousand " + oneDigit[hundredsDigit] + " hundred " + oneDigit[oneNumber];
			}
			if( hundredsDigit === 0 && thousandDigit === 0 && tenThousandDigit === 0 ){
				return oneDigit[hundredThousandDigit] + " Hundred " + " Thousand " + tens[byTensNumber] +" " +oneDigit[oneNumber];
			}
			if( byTensNumber === 0  && thousandDigit === 0 && tenThousandDigit === 0 ){
				return oneDigit[hundredThousandDigit] + " Hundred Thousand " + tens[byTensNumber] +" " +oneDigit[oneNumber];
			}
			if(byTensNumber === 0 && hundredsDigit === 0 && thousandDigit === 0 ){
				return oneDigit[hundredThousandDigit] + " Hundred " + tens[tenThousandDigit] + " Thousand " + oneDigit[oneNumber];
			}
			if(byTensNumber === 0 && hundredsDigit === 0){
				return oneDigit[hundredThousandDigit] + " Hundred " + tens[tenThousandDigit] + " " + oneDigit[thousandDigit] + " thousand " + " " +oneDigit[oneNumber];
			}
			if(byTensNumber === 0 && thousandDigit === 0){
				return oneDigit[hundredThousandDigit] + " Hundred " + tens[tenThousandDigit] + " " + oneDigit[hundredsDigit] + " hundred " + " " +oneDigit[oneNumber];
			}
			if(hundredsDigit === 0 && thousandDigit === 0){
				return oneDigit[hundredThousandDigit] + " Hundred " + tens[tenThousandDigit] + " Thousand " + tens[byTensNumber] + " " +oneDigit[oneNumber];
			}
			if(tenThousandDigit === 0 && thousandDigit === 0){
				return oneDigit[hundredThousandDigit] + " Hundred Thousand " + tens[byTensNumber] + " " +oneDigit[oneNumber];
			}
			if(byTensNumber === 0){
				return oneDigit[hundredThousandDigit] + " Hundred " + tens[tenThousandDigit] + " " + oneDigit[thousandDigit] + " thousand " + oneDigit[hundredsDigit] + " hundred " + " " + oneDigit[oneNumber];
			}
			if(hundredsDigit === 0){
				return oneDigit[hundredThousandDigit] + " Hundred " + tens[tenThousandDigit] + " " + oneDigit[thousandDigit] + " thousand " + tens[byTensNumber] + " " +oneDigit[oneNumber];
			}
			if(thousandDigit === 0){
				return oneDigit[hundredThousandDigit] + " Hundred " + tens[tenThousandDigit] + " " + oneDigit[hundredsDigit] + " hundred " + tens[byTensNumber] + " " +oneDigit[oneNumber];
			}
			else{
				return oneDigit[hundredThousandDigit] + " Hundred " + tens[tenThousandDigit] + " " + oneDigit[thousandDigit] + " Thousand " + oneDigit[hundredsDigit] + " Hundred " + tens[byTensNumber] + " " +oneDigit[oneNumber];
			}
	}

// For 9,999,999
	if( number < 10000000 ){
		const sixDigit = number % 1000000;
		const fiveDigit = number % 100000;
		const fourDigit = number % 10000;
		const threeDigit = number % 1000;
		const twoDigits	= number % 100;
		const oneNumber = number % 10;
		const millionNoRemainder = number - sixDigit;
		const millionNumber = millionNoRemainder / 1000000;
		const hundredThousandNoRemainder = sixDigit - fiveDigit;
		const hundredThousandDigit = hundredThousandNoRemainder / 100000;
		const tenThousandNoRemainder = fiveDigit - fourDigit;
		const tenThousandDigit = tenThousandNoRemainder / 10000;
		const thousandNoRemainder = fourDigit - threeDigit;
		const thousandDigit = thousandNoRemainder / 1000;
		const hundredsNoRemainder = threeDigit - twoDigits;
		const hundredsDigit = hundredsNoRemainder / 100;
		const byTenNoRemainer = twoDigits - oneNumber;
		const byTensNumber = byTenNoRemainer / 10;
			if( twoDigits > 20){
				return oneDigit [millionNumber] + " Million " + oneDigit[hundredThousandDigit] + " Hundred " + tens[tenThousandDigit] + " " + oneDigit[thousandDigit] + " Thousand " + oneDigit[hundredsDigit] + " Hundred " + tens[byTensNumber] + " " +oneDigit[oneNumber];
			}
			else if ( twoDigits > 10 && twoDigits < 20){
				const teensNumber = twoDigits - 10;
				return oneDigit [millionNumber] + " Million " + oneDigit[hundredThousandDigit] + " Hundred " + tens[tenThousandDigit] + " " + oneDigit[thousandDigit] + " Thousand " + oneDigit[hundredsDigit] + " Hundred " + teens[teensNumber];
			}
		
	}

		if( number < 100000000 ){
		const sevenDigit = number % 10000000;
		const sixDigit = number % 1000000;
		const fiveDigit = number % 100000;
		const fourDigit = number % 10000;
		const threeDigit = number % 1000;
		const twoDigits	= threeDigit % 100;
		const oneNumber = twoDigits % 10;
		const tenMillionNoRemainder = number - sevenDigit;
		const tenMillionNumber = tenMillionNoRemainder / 10000000;
		const millionNoRemainder = sevenDigit - sixDigit;
		const millionNumber = millionNoRemainder / 1000000;
		const hundredThousandNoRemainder = sixDigit - fiveDigit;
		const hundredThousandDigit = hundredThousandNoRemainder / 100000;
		const tenThousandNoRemainder = fiveDigit - fourDigit;
		const tenThousandDigit = tenThousandNoRemainder / 10000;
		const thousandNoRemainder = fourDigit - threeDigit;
		const thousandDigit = thousandNoRemainder / 1000;
		const hundredsNoRemainder = threeDigit - twoDigits;
		const hundredsDigit = hundredsNoRemainder / 100;
		const byTenNoRemainer = twoDigits - oneNumber;
		const byTensNumber = byTenNoRemainer / 10;
			if( twoDigits > 20){
				return tens[tenMillionNumber] + " " + oneDigit [millionNumber] + " Million " + oneDigit[hundredThousandDigit] + " Hundred " + tens[tenThousandDigit] + " " + oneDigit[thousandDigit] + " Thousand " + oneDigit[hundredsDigit] + " Hundred " + tens[byTensNumber] + " " +oneDigit[oneNumber];
			}
			else if ( twoDigits > 10 && twoDigits < 20){
				const teensNumber = twoDigits - 10;
				return tens[tenMillionNumber] + " " + oneDigit [millionNumber] + " Million " + oneDigit[hundredThousandDigit] + " Hundred " + tens[tenThousandDigit] + " " + oneDigit[thousandDigit] + " Thousand " + oneDigit[hundredsDigit] + " Hundred " + teens[teensNumber];
			}
	}	
		
	
		if( number < 1000000000 ){
		const eightDigit = number % 100000000;
		const sevenDigit = number % 10000000;
		const sixDigit = number % 1000000;
		const fiveDigit = number % 100000;
		const fourDigit = number % 10000;
		const threeDigit = number % 1000;
		const twoDigits	= number % 100;
		const oneNumber = number % 10;
		const hundredMillionNoRemainder = number - eightDigit;
		const hundredMillionNumber = hundredMillionNoRemainder / 100000000;
		const tenMillionNoRemainder = eightDigit - sevenDigit;
		const tenMillionNumber = tenMillionNoRemainder / 10000000;
		const millionNoRemainder = sevenDigit - sixDigit;
		const millionNumber = millionNoRemainder / 1000000;
		const hundredThousandNoRemainder = sixDigit - fiveDigit;
		const hundredThousandDigit = hundredThousandNoRemainder / 100000;
		const tenThousandNoRemainder = fiveDigit - fourDigit;
		const tenThousandDigit = tenThousandNoRemainder / 10000;
		const thousandNoRemainder = fourDigit - threeDigit;
		const thousandDigit = thousandNoRemainder / 1000;
		const hundredsNoRemainder = threeDigit - twoDigits;
		const hundredsDigit = hundredsNoRemainder / 100;
		const byTenNoRemainer = twoDigits - oneNumber;
		const byTensNumber = byTenNoRemainer / 10;
			if( twoDigits > 20){
				return oneDigit[hundredMillionNumber] + " Hundred " + tens[tenMillionNumber] + " " + oneDigit [millionNumber] + " Million " + oneDigit[hundredThousandDigit] + " Hundred " + tens[tenThousandDigit] + " " + oneDigit[thousandDigit] + " Thousand " + oneDigit[hundredsDigit] + " Hundred " + tens[byTensNumber] + " " +oneDigit[oneNumber];
			}
			else if ( twoDigits > 10 && twoDigits < 20){
				const teensNumber = twoDigits - 10;
			return oneDigit[hundredMillionNumber] + " Hundred " + tens[tenMillionNumber] + " " + oneDigit [millionNumber] + " Million " + oneDigit[hundredThousandDigit] + " Hundred " + tens[tenThousandDigit] + " " + oneDigit[thousandDigit] + " Thousand " + oneDigit[hundredsDigit] + " Hundred " + teens[teensNumber];
			}
			
	}

	return "invalid number";


}